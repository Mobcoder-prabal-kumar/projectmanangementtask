import { createSlice } from "@reduxjs/toolkit"
const initialState = {
    allUsers: []
}
const AllUsersSlice = createSlice({
    name: "project",
    initialState,
    reducers: {
        profiles: (state, action) => ({
            ...state,
            allUsers: action.payload,
        }),
    },
})
export const { profiles } = AllUsersSlice.actions
export default AllUsersSlice.reducer
