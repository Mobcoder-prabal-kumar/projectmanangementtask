import { createSlice } from "@reduxjs/toolkit"
const initialState = {
    userProfileDetails:{}
}
const ProjectSlice = createSlice({
    name: "project",
    initialState,
    reducers: {
        loginReducer: (state, action) => ({
            ...state,
            userProfileDetails: action.payload,
        }),
    },
})
export const { loginReducer } = ProjectSlice.actions
export default ProjectSlice.reducer

