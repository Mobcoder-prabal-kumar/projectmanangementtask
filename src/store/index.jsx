import { configureStore, combineReducers } from '@reduxjs/toolkit'
import ProjectSlice from "./slice/projectSlice";
import { persistStore } from 'redux-persist';
import persistReducer from 'redux-persist/es/persistReducer';
import storage from 'redux-persist/lib/storage';
import allUsersSlice from './slice/allUsersSlice';
const persistConfig = {
    key: 'root',
    storage,
}

const combinereducer = combineReducers({
    loginDetail: ProjectSlice,
    profiles:allUsersSlice
});
const persistedReducer = persistReducer(persistConfig, combinereducer);

export const store = configureStore({
    reducer: persistedReducer
})
export const persistor = persistStore(store);


// export { store, persistor }
// export const persistor = persistStore(store);

// {
//     project: ProjectSlice
// }