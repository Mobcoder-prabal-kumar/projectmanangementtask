import './App.css';
import Login from './component/login/Login';
import { BrowserRouter, Routes, Route, Navigate, Outlet } from 'react-router-dom';
import EmployeeDashBoard from './component/dashboard/employeeDashBoard/EmployeeDashBoard';
import ProjectManagerDashBoard from './component/dashboard/projectManagerDashboard/ProjectManagerDashBoard';
import DashBoard from './component/dashboard/DashBoard';
import AddProject from './component/dashboard/modal/AddProject';
import ProjectTask from './component/dashboard/projectTask/ProjectTask';
import AddTask from './component/dashboard/projectTask/AddTask';

function App() {
  const RequireAuth = () => {
    const currentUser = localStorage.getItem("uniqueId")
    return currentUser ? <Outlet /> : <Navigate to="/login" />;
  };

  return (
    <BrowserRouter>
      <Routes>
        <Route element={<RequireAuth />}>
          <Route exact path='/' element={<DashBoard />} />
          <Route exact path='/projectManagerdashboard' element={<ProjectManagerDashBoard />} />
          <Route exact path='/emplyeedashboard' element={<EmployeeDashBoard />} />
          <Route exact path='/task/:id' element={<ProjectTask />} />
          <Route exact path='/addProject' element={<AddProject />} />
          <Route exact path='/addTask' element={<AddTask />} />

        </Route>
        <Route element={<Login />} path="/login" />
      </Routes>
    </BrowserRouter >
  );
}
export default App;
