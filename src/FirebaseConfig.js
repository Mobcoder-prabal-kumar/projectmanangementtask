
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAoXxF2iHsuXJhfF4PlBvqM41CXApmOkK0",
  authDomain: "projecttask-d91e7.firebaseapp.com",
  projectId: "projecttask-d91e7",
  storageBucket: "projecttask-d91e7.appspot.com",
  messagingSenderId: "555498379742",
  appId: "1:555498379742:web:d2613cda8e38627b73654f"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const database = getAuth(app)
export const db = getFirestore(app)
// export const storage = getStorage(app)