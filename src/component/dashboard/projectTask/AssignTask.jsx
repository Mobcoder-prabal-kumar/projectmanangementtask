import React, { useEffect, useState } from 'react'
import { Modal, Select } from "antd"
import { useSelector } from "react-redux"
import { doc, updateDoc } from 'firebase/firestore'
import { db } from '../../../FirebaseConfig'
const { Option } = Select
export default function AssignTask(props) {
    const { show, assignTaskId, handleCancel, getTask } = props
    const [assign, setAssign] = useState("")
    const [task, setTask] = useState("")
    const [projectName, setProjectName] = useState("")
    const [email, setEmail] = useState("")
    const [docId, setDocId] = useState("")
    const usersssss = useSelector((state) => {
        return state.profiles.allUsers
    })

    useEffect(() => {
        setEmail(assignTaskId?.email)
        setProjectName(assignTaskId?.projectName)
        setTask(assignTaskId?.task)
        setDocId(assignTaskId?.id)
    }, [show])

    const handleassign = (e) => {
        setAssign(e)
    }
    const handleSubmit = async () => {
        if (assign) {
            const tasks = doc(db, "tasks", docId)

            const newTask = { email: email, projectName: projectName, task: task, assignTo: assign }
            await updateDoc(tasks, newTask);
            getTask()
            handleCancel()
        }
        else {
            alert("field is required")
        }
    }
    return (
        <>
            <Modal title="Edit Task" open={show} onCancel={handleCancel}>
                <div className='addProjects_section'>
                    <div className="login-form">
                        <label htmlFor="task">Assign to:</label>
                        <Select name="" id="" onChange={handleassign}>
                            {usersssss.length > 0 ? usersssss.map((data) => (
                                <Option key={data?.id} value={data.email}>{data.email}</Option>
                            )) : ''}
                        </Select>
                        <div className='buttons'>
                            <button className='add_project_button_submit' onClick={handleSubmit}>Assign</button>
                            <button className='add_project_button_submit' onClick={handleCancel} >Cancel</button>

                        </div>
                    </div>
                </div>
            </Modal>
        </>
    )
}
