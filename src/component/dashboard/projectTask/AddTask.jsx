import { addDoc, collection } from 'firebase/firestore'
import React, { useState } from 'react'
import { db } from '../../../FirebaseConfig'
import { Link } from "react-router-dom"
import { useSelector } from "react-redux"

export default function AddTask() {
    const [addtask, setAddTask] = useState([])
    const id = useSelector((state) => {
        return state?.loginDetail?.userProfileDetails?.user?.uid
    })
    const handleTask = (e) => {
        setAddTask(e.target.value)
    }
    const handleSubmit = () => {
        if (addtask) {
            addDoc(collection(db, 'tasks'), {
                task: addtask,
                email: localStorage.getItem("email"),
                projectName: localStorage.getItem("projectName")
            });
            alert("Task added successfully")
            setAddTask("")
        }
        else {
            alert("Project name and descriptin required")
        }
    }
    return (
        <div className='container'>
            <h2>{id == "wp85haKEuwN98kGgu3cTuc3gYax2" ? <Link to={'/projectManagerdashboard'}> projectManager Dashboard</Link> : <Link to={'/emplyeedashboard'}> employee Dashboard</Link>}/Add Task</h2>
            <div className='addProjects_section'>
                <div className="login-form">
                    <h2>Add Task</h2>
                    <label htmlFor="task">Task:</label>
                    <textarea rows='5' type="text" id="task" name="task" value={addtask} onChange={handleTask} />
                    <button className='add_project_button_submit' onClick={handleSubmit}>Add Task</button>
                </div>
            </div>
        </div>
    )
}
