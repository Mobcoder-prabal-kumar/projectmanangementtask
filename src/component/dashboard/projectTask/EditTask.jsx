import React, { useEffect, useState } from 'react'
import { Modal } from "antd"
import { doc, updateDoc } from 'firebase/firestore'
import { db } from '../../../FirebaseConfig'
export default function EditTask(props) {
    const { show, handleCancel, editData,getTask } = props
    const [task, setTask] = useState("")
    const [projectName, setProjectName] = useState("")
    const [email, setEmail] = useState("")
    const [docId, setDocId] = useState("")
    useEffect(() => {
        setEmail(editData?.email)
        setProjectName(editData?.projectName)
        setTask(editData?.task)
        setDocId(editData?.id)
    }, [show])

    const handleSubmit = async () => {
        if (task) {
            const tasks = doc(db, "tasks", docId)
            const newTask = { email: email, projectName: projectName, task: task }
            await updateDoc(tasks, newTask);
            getTask()
            handleCancel()
        }
        else {
            alert("field is required")
        }
    }

    const handleTaskChange = (e) => {
        setTask(e.target.value)
    }
    return (
        <>
            <Modal title="Edit Task" open={show} onCancel={handleCancel}>
                <div className='addProjects_section'>
                    <div className="login-form">
                        <label htmlFor="task">Task:</label>
                        <textarea rows='5' type="text" id="task" name="task" value={task} onChange={handleTaskChange} />
                        <div className='buttons'>
                            <button className='add_project_button_submit' onClick={handleSubmit}>Edit Task</button>
                            <button className='add_project_button_submit' onClick={handleCancel} >Cancel</button>

                        </div>
                    </div>
                </div>
            </Modal>
        </>
    )
}
// value={addtask} onChange={handleTask}onClick={handleSubmit}