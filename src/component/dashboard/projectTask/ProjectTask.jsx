import { collection, deleteDoc, doc, getDocs, query, where } from 'firebase/firestore'
import React, { useEffect, useState } from 'react'
import { db } from '../../../FirebaseConfig'
import { useParams, Link, useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from "react-redux"
import EditTask from './EditTask'
import AssignTask from './AssignTask'
import { profiles } from '../../../store/slice/allUsersSlice'

export default function ProjectTask() {
    const [projects, setProjects] = useState([])
    const [tasks, setTasks] = useState([])
    const [openEditTaskModal, setOpenEditTaskModal] = useState(false)
    const [assignTaskId, setAssignTaskId] = useState({})
    const [openAssignTaskModal, setOpenAssignEditTaskModal] = useState(false)
    const [editData, setEditData] = useState({})
    let { id } = useParams()
    const dispatch = useDispatch()
    const userId = useSelector((state) => {
        return state?.loginDetail?.userProfileDetails?.user?.uid
    })

    const usersssss = useSelector((state) => {
        return state
    })
    const navigate = useNavigate()

    useEffect(() => {
        getProjects()
        getTask()
    }, [])

    useEffect(() => {
        getUsers()
    })

    const getUsers = async () => {
        try {
            const querySnapshot = await getDocs(collection(db, 'users'));
            const data = querySnapshot.docs.map((doc) => ({ ...doc.data(), id: doc?.id }))
            dispatch(profiles(data))

        } catch (error) {
            console.error('Error fetching data:', error);
        }
    }
    const getProjects = async () => {
        try {
            const q = query(collection(db, "projects"), where("projectName", "==", id));
            const querySnapshot = await getDocs(q);
            const data = querySnapshot.docs.map((doc) => ({ ...doc.data(), id: doc?.id }))
            setProjects(data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    }


    const getTask = async () => {
        try {
            const q = query(collection(db, "tasks"), where("projectName", "==", id));
            const querySnapshot = await getDocs(q);
            const data = querySnapshot.docs.map((doc) => ({ ...doc.data(), id: doc?.id }))
            setTasks(data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    }

    const handleAddTask = () => {
        navigate("/addTask")
        getUsers()
        localStorage.setItem("projectName", projects[0]?.projectName)
    }


    const handleDelete = async (taskId) => {
        const docIdToDelete = taskId; // Replace with the actual document ID
        deleteDocument(docIdToDelete);

    }
    const deleteDocument = async (taskId) => {
        try {
            console.log("taskId", taskId)
            const taksDocs = doc(db, "tasks", taskId);
            await deleteDoc(taksDocs)
            getTask()
        } catch (error) {
            console.log(error)
        }
    };

    const handleOpenEdit = (data) => {
        setOpenEditTaskModal(true)
        setEditData(data)
    }
    const handleOpenAssign = (data) => {
        setOpenAssignEditTaskModal(true)
        setAssignTaskId(data)
    }
    const handleCancel = () => {
        setOpenEditTaskModal(false)
        setEditData({})
        setOpenAssignEditTaskModal(false)
        setAssignTaskId({})
    }
    return (
        <div className='container'>
            <div className='add_project_button'>
                <button onClick={handleAddTask}>Add Task</button>
            </div>
            <h2 >{userId == "wp85haKEuwN98kGgu3cTuc3gYax2" ? <Link to={'/projectManagerdashboard'}> projectManager Dashboard</Link> : <Link to={'/emplyeedashboard'}> employee Dashboard</Link>}/{projects[0]?.projectName}</h2>
            <table>
                <thead>
                    <tr>
                        <th>Project Name</th>
                        <th>By</th>
                        <th>Task</th>
                        <th>Assign To</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {tasks?.length > 0 ? tasks.map((data, i) => (

                        <tr key={i}>
                            <td>{data?.projectName}</td>
                            <td>{data?.email}</td>
                            <td>{data?.task}</td>
                            <td>{data?.assignTo}</td>
                            <td className='buttons'>
                                <button onClick={() => handleOpenEdit(data)}> Edit</button>
                                {userId == "wp85haKEuwN98kGgu3cTuc3gYax2" ? <button onClick={() => handleOpenAssign(data)}> assign</button> : ""}


                                <button onClick={() => handleDelete(data?.id)}>Delete</button></td>
                        </tr>
                    ))
                        : "no data"}
                </tbody>
            </table>
            <EditTask
                getTask={getTask}
                show={openEditTaskModal}
                editData={editData}
                handleCancel={handleCancel}
            />
            <AssignTask
                getTask={getTask}
                show={openAssignTaskModal}
                assignTaskId={assignTaskId}
                handleCancel={handleCancel} />
        </div>
    )
}
