import React, { useEffect } from 'react'
import ProjectManagerDashBoard from './projectManagerDashboard/ProjectManagerDashBoard'
import EmployeeDashBoard from './employeeDashBoard/EmployeeDashBoard'
import { useSelector } from "react-redux"
import { collection, getDocs } from 'firebase/firestore'
import { db } from '../../FirebaseConfig'

import { useDispatch } from 'react-redux'
import { profiles } from '../../store/slice/allUsersSlice'
export default function DashBoard() {
    const dispatch = useDispatch()

    const uniqueId = useSelector((state) => {
        return state?.loginDetail?.userProfileDetails?.user?.uid
    })

    useEffect(() => {
        getUsers()
    })

    const getUsers = async () => {
        try {
            const querySnapshot = await getDocs(collection(db, 'users'));
            const data = querySnapshot.docs.map((doc) => ({ ...doc.data(), id: doc?.id }))
            dispatch(profiles(data))

        } catch (error) {
            console.error('Error fetching data:', error);
        }
    }

    return (
        <div className='container'>
            {
                uniqueId == "wp85haKEuwN98kGgu3cTuc3gYax2"
                    ?
                    <ProjectManagerDashBoard /> :
                    <EmployeeDashBoard />
            }
        </div>
    )
}
