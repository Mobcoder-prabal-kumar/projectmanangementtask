import React, { useEffect, useState } from 'react'
import './projectManagerDashBoard.css'
import { collection, deleteDoc, doc, getDocs } from 'firebase/firestore'
import { db } from '../../../FirebaseConfig'
import { Link, useNavigate } from 'react-router-dom'
import Logout from '../../logout/Logout'
import { useDispatch } from "react-redux"
const url = "/task/"
export default function ProjectManagerDashBoard() {
    const navigate = useNavigate()
    const [projects, setProjects] = useState()
    useEffect(() => {
        getProjects()
    }, [])

    const getProjects = async () => {
        try {
            const querySnapshot = await getDocs(collection(db, 'projects'));
            const data = querySnapshot.docs.map((doc) => ({ ...doc.data(), id: doc?.id }))
            setProjects(data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    }
    const handleAddProject = () => {
        navigate('/addProject')
    }
    const handleProjectDelete = async (id) => {
        const projectDocs = doc(db, "projects", id);
        await deleteDoc(projectDocs)
        getProjects()
    }
    const handleLogout = () => {
        localStorage.clear()
        navigate('/login')
    }
    return (
        <div className='container'>
            <Logout
                handleLogout={handleLogout}
            />
            <h1 className='header'>
                project Manager Dashboard
            </h1>
            <div className='add_project_button'>
                <button onClick={handleAddProject}>Add Project</button>
            </div>
            <table>
                <thead>
                    <tr>
                        <th>Project Name</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {projects?.length > 0 ? projects.map((data, i) => (

                        <tr key={i}>
                            <td className='blue cr_p'><Link
                                to={{
                                    pathname: `${url}${data.projectName}`,
                                }}
                            >{data?.projectName}</Link></td>
                            <td>{data?.description}</td>
                            <td><button onClick={() => handleProjectDelete(data?.id)}>Delete</button></td>
                        </tr>
                    ))
                        : ""}
                </tbody>
            </table>
        </div>
    )
}
