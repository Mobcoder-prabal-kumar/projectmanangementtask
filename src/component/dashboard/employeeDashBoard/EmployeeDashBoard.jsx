import { collection, getDocs } from 'firebase/firestore'
import React, { useEffect, useState } from 'react'
import { useSelector } from "react-redux"
import { useNavigate, Link } from "react-router-dom"
import { db } from '../../../FirebaseConfig'
import Logout from '../../logout/Logout'
const url = "/task/"

export default function EmployeeDashBoard() {
    const selector = useSelector((state) => {
        return state
    })
    const navigate = useNavigate()
    const [projects, setProjects] = useState()

    useEffect(() => {
        getProjects()
    }, [])

    const getProjects = async () => {
        try {
            const querySnapshot = await getDocs(collection(db, 'projects'));
            const data = querySnapshot.docs.map((doc) => ({ ...doc.data(), id: doc?.id }))
            setProjects(data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    }

    const handleLogout = () => {
        localStorage.clear()
        navigate('/login')
    }

    return (
        <div className='container'>
            <Logout
                handleLogout={handleLogout}
            />
            <h1 className='header'>
                Employee Dashboard
            </h1>
            <table>
                <thead>
                    <tr>
                        <th>Project Name</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    {projects?.length > 0 ? projects.map((data, i) => (

                        <tr key={i}>
                            <td className='blue cr_p'><Link
                                to={{
                                    pathname: `${url}${data.projectName}`,
                                }}
                            >{data?.projectName}</Link></td>
                            <td>{data?.description}</td>
                        </tr>
                    ))
                        : ""}
                </tbody>
            </table>
        </div>
    )
}
