import React, { useState } from 'react'
import "./addProjects.css"
import { addDoc, collection } from 'firebase/firestore'
import { db } from '../../../FirebaseConfig'
import { useNavigate, Link } from 'react-router-dom'
export default function AddProject() {
  const [projectName, setProjectName] = useState("")
  const [projectDescription, setProjectDescription] = useState("")
  const navigate = useNavigate()
  const handleSubmit = () => {
    if (projectName && projectDescription) {
      addDoc(collection(db, 'projects'), {
        projectName: projectName,
        description: projectDescription,
      });
      setProjectName("")
      setProjectDescription("")
      navigate('/projectManagerdashboard')
    }
    else {
      alert("Project name and descriptin required")
    }
  }
  const handleProjectNameChange = (e) => {
    setProjectName(e.target.value)
  }
  const handleDescription = (e) => {
    setProjectDescription(e.target.value)

  }
  return (
    <div className='container'>
      <h2><Link to={'/projectManagerdashboard'}> projectManager Dashboard</Link>/Add Projects</h2>
      <div className='addProjects_section'>
        <div className="login-form">
          <h2>Login</h2>
          <label htmlFor="projectName">project Name:</label>
          <input type="email" id="projectName" name="projectName" value={projectName} onChange={handleProjectNameChange} />

          <label htmlFor="passdescriptionword">Description:</label>
          <textarea rows='5' type="text" id="description" name="description" value={projectDescription} onChange={handleDescription} />
          <button className='add_project_button_submit' onClick={handleSubmit}>Add Projects</button>
        </div>
      </div>
    </div>
  )
}
