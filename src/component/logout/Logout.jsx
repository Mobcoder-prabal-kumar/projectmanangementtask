import React from 'react'

export default function Logout(props) {
    const { handleLogout } = props
    return (
        <>
            <div className='logout_button'>
                <button onClick={handleLogout}>
                    Logout
                </button>
            </div>
        </>
    )
}
