export const validation = (props) => {
    const { email, password, setPasswordErrorMsg, setEmailErrorMsg } = props
    const regx = /^([^\S\.]{0,})+(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})+([^\S\.]{0,})$/;
    let validateEmail = regx.test(String(email).toLowerCase());
    let validate = true
    if (email == "" || email == undefined || email == null) {
        validate = false
        setEmailErrorMsg("Email is required")
    }
    else if (!validateEmail) {
        validate = false
        setEmailErrorMsg("Please enter valid email")

    }
    else {
        setEmailErrorMsg("")
    }
    if (password == "" || password == undefined || password == null) {
        validate = false
        setPasswordErrorMsg("password is required")
    }
    else if (password < 6) {
        validate = false
        setPasswordErrorMsg("Password should be greater then 6")
    }
    else {
        setPasswordErrorMsg("")
    }
    return validate
}