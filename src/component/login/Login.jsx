import React, { useEffect, useState } from 'react'
import './login.css'
import { useNavigate } from 'react-router-dom'
import { database, db } from '../../FirebaseConfig';
import { createUserWithEmailAndPassword, signInWithEmailAndPassword } from 'firebase/auth'
import { validation } from './loginValidation';
import { useDispatch } from 'react-redux'
import { loginReducer } from '../../store/slice/projectSlice';
import { addDoc, collection, getDocs } from 'firebase/firestore';
import { profiles } from '../../store/slice/allUsersSlice';
export default function Login() {
  const dispatch = useDispatch()
  const [email, setEmail] = useState("")
  const [emailErrorMsg, setEmailErrorMsg] = useState("")
  const [password, setPassword] = useState("")
  const [passwordErrorMsg, setPasswordErrorMsg] = useState("")
  const [login, setLogin] = useState(true)
  const [signup, setSignup] = useState(false)
  const navigate = useNavigate();
  const handleEmailChange = (e) => {
    setEmail(e.target.value)
    setEmailErrorMsg("")
  }

  const handlePasswordChange = (e) => {
    setPassword(e.target.value)
    setPasswordErrorMsg("")
  }

  const handleLoginSignUp = () => {
    setLogin(!login)
    setSignup(!signup)
  }

  useEffect(() => {
    getUsers()
  })

  const getUsers = async () => {
    try {
      const querySnapshot = await getDocs(collection(db, 'users'));
      const data = querySnapshot.docs.map((doc) => ({ ...doc.data(), id: doc?.id }))
      dispatch(profiles(data))

    } catch (error) {
      console.error('Error fetching data:', error);
    }
  }



  const handleLogin = (e) => {
    e.preventDefault();
    if (validation({
      email: email,
      password: password,
      setEmailErrorMsg: setEmailErrorMsg,
      setPasswordErrorMsg: setPasswordErrorMsg,
    })) {
      signInWithEmailAndPassword(database, email, password).then(data => {
        localStorage.setItem("email", data?.user?.email)
        localStorage.setItem("uniqueId", data?.user?.uid)
        dispatch(loginReducer(data))
        getUsers()
        navigate('/')
      }).catch(err => {
        console.log("errrrr", err)
        alert("Please enter valid email and password")
      })
    }

  }

  const handleSignUp = (e) => {
    e.preventDefault();
    if (validation({
      email: email,
      password: password,
      setEmailErrorMsg: setEmailErrorMsg,
      setPasswordErrorMsg: setPasswordErrorMsg,
    })) {
      createUserWithEmailAndPassword(database, email, password).then(data => {
        addDoc(collection(db, 'users'), {
          email: email,
          isProjectManager: 2,
        });
        alert("signup Successfully please select login and enter user name password")
        setEmail("")
        setPassword("")
      }).catch(err => {
        console.log("errrrr", err)
        alert("Please enter valid email and password")
      })
    }

  }
  return (
    <div className='login_section'>
      <div className='login_div container'>
        <div className="login-form">
          <h2><span className={login ? "blue cr_p" : "cr_p"} onClick={handleLoginSignUp}>Login</span>/<span className={signup ? "blue cr_p" : "cr_p"} onClick={handleLoginSignUp}>signup</span></h2>
          <label htmlFor="username">Email:</label>
          <input type="email" id="email" name="email" value={email} onChange={handleEmailChange} />
          {emailErrorMsg ? <span className='error_msg'>{emailErrorMsg}</span> : ""}
          <label htmlFor="password">Password:</label>
          <input type="text" id="password" name="password" value={password} onChange={handlePasswordChange} />
          {passwordErrorMsg ? <span className='error_msg'>{passwordErrorMsg}</span> : ""}
          <button onClick={login ? handleLogin : handleSignUp}>{login ? "Login" : "signup"}</button>
        </div>
      </div>
    </div >
  )
}
